package mk.ukim.finki.emt.eshop.config;

import lombok.AllArgsConstructor;
import mk.ukim.finki.emt.eshop.model.User;
import mk.ukim.finki.emt.eshop.model.enumerations.Role;
import mk.ukim.finki.emt.eshop.repository.UserRepository;
import mk.ukim.finki.emt.eshop.service.UserService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
@AllArgsConstructor
public class DataInitializer {

    private UserRepository userRepository;

    private PasswordEncoder passwordEncoder;

    private void createUsers() {
        String username1 = "111111";
        User user = new User();
        user.setUsername(username1);
        user.setPassword(passwordEncoder.encode(username1));
        user.setName(username1);
        user.setSurname(username1);
        user.setRole(Role.ROLE_ADMIN);
        userRepository.save(user);

        String username2 = "222222";
        User user2 = new User();
        user2.setUsername(username2);
        user2.setPassword(passwordEncoder.encode(username2));
        user2.setName(username2);
        user2.setSurname(username2);
        user2.setRole(Role.ROLE_USER);

        userRepository.save(user2);

    }

    @PostConstruct
    public void init() {
        List<User> usersList = userRepository.findAll();
        if (usersList.size()==0) {
            createUsers();
        }

    }
}
